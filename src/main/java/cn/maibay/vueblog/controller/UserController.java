package cn.maibay.vueblog.controller;


import cn.maibay.vueblog.common.lang.Result;
import cn.maibay.vueblog.entity.User;
import cn.maibay.vueblog.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zrl
 * @since 2020-06-26
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;

    @GetMapping("/{id}")
    public Object test(@PathVariable("id") long id){
        //System.out.println(id+"-------");
        User o= userService.getById(id);
        //System.out.println(o+"....................");
        return Result.succ(o);
    }
}
