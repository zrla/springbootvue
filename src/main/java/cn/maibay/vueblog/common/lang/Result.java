package cn.maibay.vueblog.common.lang;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable{
    private String code;
    private String msg;
    private T data;

    public static<T> Result<T> succ(T data){
        return succ("操作成功！",data);
    }
    public static<T> Result<T> succ(String msg,T data){
        Result<T> m = new Result<>();
        m.setCode("0");
        m.setData(data);
        m.setMsg(msg);
        return m;
    }
}
