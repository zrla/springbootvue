package cn.maibay.vueblog.service.impl;

import cn.maibay.vueblog.entity.Blog;
import cn.maibay.vueblog.mapper.BlogMapper;
import cn.maibay.vueblog.service.IBlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zrl
 * @since 2020-06-26
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements IBlogService {

}
