package cn.maibay.vueblog.service.impl;

import cn.maibay.vueblog.entity.User;
import cn.maibay.vueblog.mapper.UserMapper;
import cn.maibay.vueblog.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zrl
 * @since 2020-06-26
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
