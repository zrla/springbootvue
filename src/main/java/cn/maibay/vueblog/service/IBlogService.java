package cn.maibay.vueblog.service;

import cn.maibay.vueblog.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zrl
 * @since 2020-06-26
 */
public interface IBlogService extends IService<Blog> {

}
