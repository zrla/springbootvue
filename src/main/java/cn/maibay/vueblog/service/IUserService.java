package cn.maibay.vueblog.service;

import cn.maibay.vueblog.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zrl
 * @since 2020-06-26
 */
public interface IUserService extends IService<User> {

}
