package cn.maibay.vueblog.mapper;

import cn.maibay.vueblog.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zrl
 * @since 2020-06-26
 */
public interface BlogMapper extends BaseMapper<Blog> {

}
